#This code is for parsing the articles from internet using google's ranking system. 
import requests
from bs4 import BeautifulSoup

#Here we are finding articles that are relavant to the prompt using google search engine
def search_google(query):
    search_url = f"https://www.google.com/search?q={query}"
    response = requests.get(search_url)
    soup = BeautifulSoup(response.text, "html.parser")
    search_results = soup.find_all("div", class_="tF2Cxc")
    print(search_results)
    return search_results

#Here we are extracting the text from the url of each article
def get_article_text(url):
    response = requests.get(url)
    article_soup = BeautifulSoup(response.text, "html.parser")
    paragraphs = article_soup.find_all("p")
    article_text = " ".join([p.get_text() for p in paragraphs])
    return article_text

def parse(prompt):
    search_results = search_google(prompt)
    articles_text = []
    print("Hey")
    for result in search_results[:5]:  # Getting only the first five ranked articles
        url = result.a["href"]
        article_text = get_article_text(url)
        articles_text.append(article_text)
    
    return articles_text

user_prompt = "Machine learning in healthcare"
parsed_articles = parse(user_prompt)

for idx, article_text in enumerate(parsed_articles):
    print(f"Article {idx + 1} Text:")
    print(article_text)
    print("-" * 50)
