#This code is for parsing the articles from internet using google's ranking system. 
import requests
from bs4 import BeautifulSoup

#Here we are finding articles that are relavant to the prompt using google search engine
def search_google(query):
    search_url = f"https://www.google.com/search?q={query}"
    response = requests.get(search_url)
    soup = BeautifulSoup(response.text, "html.parser")

    # Find all anchor elements that contain the links
    article_links = soup.find_all('a', href=True)

    # Extract the URLs from the href attributes
    relevant_urls = []
    for link in article_links:
        url = link['href']
        # Filter out non-article links and keep those starting with "http" or "https"
        if url.startswith("/url"):
            # Exclude certain types of URLs that are not articles       
            relevant_urls.append(url)

    return relevant_urls    


def parse(prompt):
    search_results = search_google(prompt)
    articles_url = []

    for result in search_results[:10]:  # Getting only the first five ranked articles
        if result.startswith('/url?q='):
            encoded_url = result[7:]
            actual_url = requests.utils.unquote(encoded_url.split('&sa=U&')[0])  # Filtering the URL from the parsed unfiltered URL
            
            # we are checking if the URL contains "scholar", if not, fetch the article url because google scholar links comes up in result
            if 'scholar' not in actual_url:
                articles_url.append(actual_url)

    return articles_url



