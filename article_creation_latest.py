# This code is for generating contents from the links using OpenAI
import openai 
import googleParsing

openai.api_key = "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"

# System message to get the correct Json content
System_message = """You are a content creator bot who will help content creators to help create contents for the asked prompt by referring to the links below. 
You will not talk about source of the content, you will only create content that the prompt is mentioning. Most important the output should only have Json output """

# modifying the list of links into a text, so that we can pass it to API
def links(article_links):
    text=""
    for link in article_links:
        text = text + "Link: "
        text = text + link
        text = text + '\n'
        text = text + "*******************" 
        text = text + '\n'
    return text

# Calling OpenAI API and generating content in text Format, we can get in Json format in future if there is a need
def generate_content(actual_links):
    response = openai.Completion.create(
        engine = "text-davinci-003",
        prompt = System_message + links(actual_links),
        max_token = 100,
        temperature = 0.0
    )
    result_text = response.choices[0].text.strip()

    return result_text

# We will be getting this user prompt from frontend
user_prompt = "Latest news about chess world cup"
article_links = googleParsing.parse(user_prompt)
output_text = generate_content(article_links)

